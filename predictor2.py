#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  predictor.py
#  
#  Copyright 2021 eddiewrc <eddiewrc@alnilam>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import os, sys
import torch as t
import numpy as np
import NNwrapper as NN
import captum.attr as CA
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve, auc, average_precision_score, precision_recall_curve

def readIndices(dirName):
	files = os.listdir(dirName)
	folds = []
	for i, f in enumerate(files):
		ifp = open(dirName+f)
		lines = ifp.readlines()
		ifp.close()
		tmp = []
		for l in lines:
			tmp.append(int(float(l.strip())))
		folds.append(tmp)
	return folds

def parseLine(line):
	tmp = []
	for l in line:
		tmp.append(float(l))
	return tmp

def readFeatures(f):
	x = []
	y = []
	ifp = open(f)
	lines = ifp.readlines()
	ifp.close()
	for l in lines:
		tmp = l.strip().split(",")
		x.append(parseLine(tmp[:-1]))
		y.append(int(float(tmp[-1])))
	return x, y

def buildVectors(fold, xdb, ydb):
	x = []
	y = []
	for f in fold:
		x.append(xdb[f])
		y.append(ydb[f])
	return x, y

def main(args):
	#names = ["ring_1000samples-2feat.csv","ring_1000samples-4feat.csv","ring_1000samples-8feat.csv","ring_1000samples-16feat.csv","ring_1000samples-32feat.csv","ring_1000samples-64feat.csv","ring_1000samples-128feat.csv","ring_1000samples-256feat.csv","ring_1000samples-512feat.csv"]
	#ofp = open("resultsRING.txt","a", 1)
	names = ["xor_1000samples-2feat.csv","xor_1000samples-4feat.csv","xor_1000samples-8feat.csv","xor_1000samples-16feat.csv","xor_1000samples-32feat.csv","xor_1000samples-64feat.csv","xor_1000samples-128feat.csv","xor_1000samples-256feat.csv","xor_1000samples-512feat.csv"]
	ofp = open("resultsXOR.txt","a", 1)
	#names = ["ring+xor_1000samples-2feat.csv","ring+xor_1000samples-4feat.csv","ring+xor_1000samples-8feat.csv","ring+xor_1000samples-16feat.csv","ring+xor_1000samples-32feat.csv","ring+xor_1000samples-64feat.csv","ring+xor_1000samples-128feat.csv","ring+xor_1000samples-256feat.csv","ring+xor_1000samples-512feat.csv"]
	#ofp = open("resultsRING+XOR.txt","a", 1)

	SAmethods = ["IG_noMul", "Saliency" , "DeepLift", "InputXGradient",  "SmoothGrad" , "GuidedBackprop" , "Deconvolution",  "FeatureAblation" , "FeaturePermutation" , "ShapleyValueSampling" ]
	realFeatPos = [0,1]
	ofp.write("Dataset\tAUC\tAUPRC\t")
	for n in SAmethods:
		ofp.write(n+"_bestK\t"+n+"_best2K\t")
	ofp.write("\n")
	for n in names:
		auc, auprc, attrDB = mainPred(n, SAmethods, realFeatPos)
		ofp.write("%s\t%.3f\t%.3f\t" % (n,auc, auprc))
		for n in SAmethods:
			ofp.write("%.3f\t%.3f\t"% (np.mean(attrDB[n]["k"]), np.mean(attrDB[n]["2k"])))
		ofp.write("\n")
	
		


def mainPred(NAME, SAmethods, realFeatPos):
	print("#################################################", NAME)
	folds = readIndices("indices/")
	CV_FOLDS = len(folds)
	xdb, ydb = readFeatures("features/"+NAME)
	i = 0
	aucList = []
	auprcList = []
	kList = []
	k2List = []
	attribDB = {}

	while i < CV_FOLDS:
		test = folds.pop(0)
		train = [ x for f in  folds for x in f]
		X, Y = buildVectors(train, xdb, ydb)
		print(len(test), len(train))
		model = NN.Baseline(len(X[0]), name = "prova1")
		wrapper = NN.NNwrapper(model)
		wrapper.fit(X, Y, "cpu", epochs=200)
		Yp = wrapper.predict(X, "cpu")
		getScoresSVR(Yp, Y, PRINT=True)
		x, y = buildVectors(test, xdb, ydb)
		yp = wrapper.predict(x, "cpu")

		sen, spe, acc, bac, pre, mcc, aucScore, auprc = getScoresSVR(yp, y, PRINT=True)
		aucList.append(aucScore)
		auprcList.append(auprc)
		folds.append(test)
		#ig = IntegratedGradients(model)
		for attribName in SAmethods:
			print("Working on ", attribName)
			if not attribName in attribDB:
				attribDB[attribName] = {"k":[], "2k":[]}
			tmpx = t.tensor(x)
			#print(tmpx.size())
			tmpx.requires_grad_()
			if "IG_noMul" in attribName:
				ig = CA.IntegratedGradients(model, multiply_by_inputs=False)
				attr = ig.attribute(tmpx, target=0, return_convergence_delta=False)
			if "Saliency" in attribName:
				ig = CA.Saliency(model)
				attr = ig.attribute(tmpx, target=0, abs=True)
			if "DeepLift" in attribName:
				ig = CA.DeepLift(model, multiply_by_inputs=False)
				attr = ig.attribute(tmpx, target=0, return_convergence_delta=False)
			if "InputXGradient" in attribName:
				ig = CA.InputXGradient(model)
				attr = ig.attribute(tmpx, target=0)	
			if "SmoothGrad" in attribName:
				ig = CA.NoiseTunnel(CA.Saliency(model))
				attr = ig.attribute(tmpx, target=0, nt_samples=50)
			if "GuidedBackprop" in attribName:
				ig = CA.GuidedBackprop(model)
				attr = ig.attribute(tmpx, target=0)	
			if "Deconvolution" in attribName:
				ig = CA.Deconvolution(model)
				attr = ig.attribute(tmpx, target=0)				
			if "FeatureAblation" in attribName:
				ig = CA.FeatureAblation(model)
				attr = ig.attribute(tmpx, target=0)
			if "FeaturePermutation" in attribName:
				ig = CA.FeaturePermutation(model)
				attr = ig.attribute(tmpx, target=0)	
			if "ShapleyValueSampling" in attribName:
				ig = CA.ShapleyValueSampling(model)
				attr = ig.attribute(tmpx, target=0)			
					
			print(attr.size())
			attr = attr.detach().numpy().tolist()
			
			tmpk, tmpk2 = computeKs(attr, realFeatPos)
			attribDB[attribName]["k"].append(np.mean(tmpk))
			attribDB[attribName]["2k"].append(np.mean(tmpk2))
		i+=1
	return np.mean(aucList), np.mean(auprcList), attribDB 

def computeKs(attr, realFeatPos):	
	k = []
	k2 = []
	i = 0
	while i < len(attr):
		s = 0
		tmp = []
		while s < len(attr[i]):
			tmp.append((s, attr[i][s]))
			s += 1
		tmp = sorted(tmp, key=lambda x:x[1], reverse=True)

		k.append(findCorrectFeats(realFeatPos, tmp, 2))
		k2.append(findCorrectFeats(realFeatPos, tmp, 4))
		i+=1
	
	return k, k2

def findCorrectFeats(correctIndices, attr, pos):
	i = 0
	k = 0
	while i < min(pos, len(attr)):
		if attr[i][0] in correctIndices:
			k+=1
		i+=1
	return k / float(len(correctIndices))
	
def visualize_importances(feature_names, importances, title="Average Feature Importances", plot=True, axis_title="Features", SAVEFIG=None):
	print(title)
	for i in range(len(importances)):
		print(i, ": ", '%.3f'%(importances[i]))
	x_pos = (np.arange(len(importances)))
	pred = importances
	real = [1,1]+[0]*(len(importances)-2)
	assert len(real) == len(pred)
	if plot:
		#plt.figure(figsize=(12,6))
		#plt.bar(x_pos, importances, align='center')
#plt.xticks(x_pos, feature_names, wrap=True)
		#plt.xlabel(axis_title)
		#plt.title(title)
		precision, recall, thresholds = precision_recall_curve(real, pred)
		#plt.plot(recall, precision)
		#plt.show()
		fpr, tpr, _ = roc_curve(real, pred)		
		fig, (ax1, ax2, ax3) = plt.subplots(figsize=[10.0, 5], ncols=3)
		ax1.set_ylabel("Precision")
		ax1.set_xlabel("Recall")
		ax1.set_title("PR curve")
		ax1.set_xlim(0,1)
		ax1.set_ylim(0,1)
		ax1.plot(recall, precision)
		ax1.grid()
		ax2.plot(fpr, tpr)
		ax2.set_ylim(0,1)
		ax2.set_xlim(0,1)
		ax2.plot([0,1],[0,1],"--",c="grey",)
		ax2.set_xlabel("FPR")
		ax2.set_ylabel("TPR")
		ax2.set_title("ROC curve")
		ax2.grid()
		ax3.bar(x_pos, importances, align='center')
		#plt.xticks(x_pos, feature_names, wrap=True)
		#plt.xlabel(axis_title)
		plt.title(title)

		if SAVEFIG != None:
			plt.savefig(SAVEFIG, dpi=400)
		plt.show()
		plt.clf()

def getScoresSVR(pred, real, threshold=None, invert = False, PRINT = False, CURVES = False, SAVEFIG=None):
	print( type(pred[0]))
	if type(pred[0]) == list or type(pred[0]) == np.ndarray:
		tmp = []
		for i in pred:
			if type(i) == np.ndarray:
				i = i.flatten().tolist()
				#print i
				#raw_input()
			tmp += i
		pred = tmp
		tmp = []
		for i in real:
			tmp += i
		real = tmp
	#print pred
	import math
	if len(pred) != len(real):
		raise Exception("ERROR: input vectors have differente len!")
	if PRINT:
		print ("Computing scores for %d predictions" % len(pred)	)	
	from sklearn.metrics import roc_curve, auc, average_precision_score, precision_recall_curve
	
	if CURVES or SAVEFIG != None:
		import matplotlib.pyplot as plt		
		precision, recall, thresholds = precision_recall_curve(real, pred)
		#plt.plot(recall, precision)
		#plt.show()
		fpr, tpr, _ = roc_curve(real, pred)		
		fig, (ax1, ax2) = plt.subplots(figsize=[10.0, 5], ncols=2)
		ax1.set_ylabel("Precision")
		ax1.set_xlabel("Recall")
		ax1.set_title("PR curve")
		ax1.set_xlim(0,1)
		ax1.set_ylim(0,1)
		ax1.plot(recall, precision)
		ax1.grid()
		ax2.plot(fpr, tpr)
		ax2.set_ylim(0,1)
		ax2.set_xlim(0,1)
		ax2.plot([0,1],[0,1],"--",c="grey",)
		ax2.set_xlabel("FPR")
		ax2.set_ylabel("TPR")
		ax2.set_title("ROC curve")
		ax2.grid()
		if SAVEFIG != None:
			plt.savefig(SAVEFIG, dpi=400)
		plt.show()
		plt.clf()
		
	fpr, tpr, thresholds = roc_curve(real, pred)
	auprc = average_precision_score(real, pred)
	aucScore = auc(fpr, tpr)		
	i = 0
	r = []
	while i < len(fpr):
		r.append((fpr[i], tpr[i], thresholds[i]))
		i+=1	
	ts = sorted(r, key=lambda x:(1.0-x[0]+x[1]), reverse=True)[:3]	
	#if PRINT:
	#	print ts
	if threshold == None:
		if PRINT:
			print (" > Best threshold: " + str(ts[0][2]))
		threshold = ts[0][2]
	i = 0
	confusionMatrix = {}
	confusionMatrix["TP"] = confusionMatrix.get("TP", 0)
	confusionMatrix["FP"] = confusionMatrix.get("FP", 0)
	confusionMatrix["FN"] = confusionMatrix.get("FN", 0)
	confusionMatrix["TN"] = confusionMatrix.get("TN", 0)
	if invert == True:
		while i < len(real):
			if float(pred[i])>=threshold and (real[i]==0):
				confusionMatrix["TN"] = confusionMatrix.get("TN", 0) + 1
			if float(pred[i])>=threshold and real[i]==1:
				confusionMatrix["FN"] = confusionMatrix.get("FN", 0) + 1
			if float(pred[i])<=threshold and real[i]==1:
				confusionMatrix["TP"] = confusionMatrix.get("TP", 0) + 1
			if float(pred[i])<=threshold and real[i]==0:
				confusionMatrix["FP"] = confusionMatrix.get("FP", 0) + 1
			i += 1
	else:
		while i < len(real):
			if float(pred[i])<=threshold and (real[i]==0):
				confusionMatrix["TN"] = confusionMatrix.get("TN", 0) + 1
			if float(pred[i])<=threshold and real[i]==1:
				confusionMatrix["FN"] = confusionMatrix.get("FN", 0) + 1
			if float(pred[i])>=threshold and real[i]==1:
				confusionMatrix["TP"] = confusionMatrix.get("TP", 0) + 1
			if float(pred[i])>=threshold and real[i]==0:
				confusionMatrix["FP"] = confusionMatrix.get("FP", 0) + 1
			i += 1
	#print "--------------------------------------------"
	#print confusionMatrix["TN"],confusionMatrix["FN"],confusionMatrix["TP"],confusionMatrix["FP"]
	if PRINT:
		print ("      | DEL         | NEUT             |")
		print ("DEL   | TP: %d   | FP: %d  |" % (confusionMatrix["TP"], confusionMatrix["FP"] ))
		print ("NEUT  | FN: %d   | TN: %d  |" % (confusionMatrix["FN"], confusionMatrix["TN"]))	
	
	sen = (confusionMatrix["TP"]/max(0.00001,float((confusionMatrix["TP"] + confusionMatrix["FN"]))))
	spe = (confusionMatrix["TN"]/max(0.00001,float((confusionMatrix["TN"] + confusionMatrix["FP"]))))
	acc =  (confusionMatrix["TP"] + confusionMatrix["TN"])/max(0.00001,float((sum(confusionMatrix.values()))))
	bac = (0.5*((confusionMatrix["TP"]/max(0.00001,float((confusionMatrix["TP"] + confusionMatrix["FN"])))+(confusionMatrix["TN"]/max(0.00001,float((confusionMatrix["TN"] + confusionMatrix["FP"])))))))
	inf =((confusionMatrix["TP"]/max(0.00001,float((confusionMatrix["TP"] + confusionMatrix["FN"])))+(confusionMatrix["TN"]/max(0.00001,float((confusionMatrix["TN"] + confusionMatrix["FN"])))-1.0)))
	pre =(confusionMatrix["TP"]/max(0.00001,float((confusionMatrix["TP"] + confusionMatrix["FP"]))))
	mcc =	( ((confusionMatrix["TP"] * confusionMatrix["TN"])-(confusionMatrix["FN"] * confusionMatrix["FP"])) / max(0.00001,math.sqrt((confusionMatrix["TP"]+confusionMatrix["FP"])*(confusionMatrix["TP"]+confusionMatrix["FN"])*(confusionMatrix["TN"]+confusionMatrix["FP"])*(confusionMatrix["TN"]+confusionMatrix["FN"]))) )  
	
	if PRINT:
		print( "\nSen = %3.3f" % sen)
		print( "Spe = %3.3f" %  spe)
		print( "Acc = %3.3f " % acc)
		print( "Bac = %3.3f" %  bac)
		#print "Inf = %3.3f" % inf
		print( "Pre = %3.3f" %  pre)
		print( "MCC = %3.3f" % mcc)
		print( "#AUC = %3.3f" % aucScore)
		print( "#AUPRC= %3.3f" % auprc)
		print( "--------------------------------------------"	)
	
	return sen, spe, acc, bac, pre, mcc, aucScore, auprc

if __name__ == '__main__':
	import sys
	sys.exit(main(sys.argv))
